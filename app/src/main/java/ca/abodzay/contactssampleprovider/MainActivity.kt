package ca.abodzay.contactssampleprovider

import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var contactString = ""
        //projection defines what columns we are selecting from the underlying table
        //this projection is selecting the display name and ID of contacts
        //if we left the projection null, we would select all columns
        val projection: Array<out String> = arrayOf(
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts._ID
        )

        //The selection is like the WHERE clause of our query, here we define the condition
        //upon which we would like to select.
        //Here, we are selecting contacts who have a display name that is LIKE a parameter
        //We will need to set that parameter.
        //If left null, we select everything
        val selectionCriteria = "${ContactsContract.Contacts.DISPLAY_NAME} LIKE ?"

        //Here was set that arguments for the selection criteria above.
        //In this case, we are selecting everyone with a 'e' in their name.
        val selectionArgs = arrayOf<String>("%e%")

        //The sort order defines how things will be sorted in the cursor
        // (note the space before ASC)
        val sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " ASC"

        val cursor = contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            projection,
            selectionCriteria,
            selectionArgs,
            sortOrder) as Cursor;

                    // we could use the cursor with an AdapterView &
                    // but here we're logging only the Display Name
        while (cursor.moveToNext()) {
            var contactName: String = cursor.getString(
            cursor.getColumnIndex(
                ContactsContract.Contacts.DISPLAY_NAME));
            Log.d("CONTACT", contactName);
            contactString += contactName + "\n"
        }   // while cursor not at end
        cursor.close();
        findViewById<TextView>(R.id.tv).text = contactString
    }
}